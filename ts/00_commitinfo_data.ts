/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@gitzone/tstest',
  version: '1.0.77',
  description: 'a test utility to run tests that match test/**/*.ts'
}
