// node native
import * as path from 'path';

export { path };

// @apiglobal scope
import * as typedserver from '@apiglobal/typedserver';

export {
  typedserver
}

// @push.rocks scope
import * as consolecolor from '@push.rocks/consolecolor';
import * as smartbrowser from '@push.rocks/smartbrowser';
import * as smartdelay from '@push.rocks/smartdelay';
import * as smartfile from '@push.rocks/smartfile';
import * as smartlog from '@push.rocks/smartlog';
import * as smartpromise from '@push.rocks/smartpromise';
import * as smartshell from '@push.rocks/smartshell';
import * as tapbundle from '@push.rocks/tapbundle';

export {
  consolecolor,
  smartbrowser,
  smartdelay,
  smartfile,
  smartlog,
  smartpromise,
  smartshell,
  tapbundle,
};

// @gitzone scope
import * as tsbundle from '@gitzone/tsbundle';

export { tsbundle };

// sindresorhus
import figures from 'figures';

export { figures };
